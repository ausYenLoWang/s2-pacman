//Copyright Joshua Ferguson 2022, South Metropolitan TAFE.
//Only for educational purposes. This script cannot directly be used in assessment materials.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class should be attached to the door parent object
public class Door : MonoBehaviour
{
    private Animator anim; //tracks the door

    private void Awake()
    {
        TryGetComponent(out anim); //get animator from this object
    }

    private void OnTriggerEnter(Collider other)
    {
        //if we are able to get an Agent component from colliding object
        if (other.TryGetComponent(out Agent agent) == true)
        {
            anim.SetBool("openDoor", true); //play open open door animation
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //if we are able to get an Agent component from colliding object
        if (other.TryGetComponent(out Agent agent) == true)
        {
            anim.SetBool("openDoor", false); //play close door animation
        }
    }
}
