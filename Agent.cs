//Copyright Joshua Ferguson 2022, South Metropolitan TAFE.
//Only for educational purposes. This script cannot directly be used in assessment materials.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Agent : MonoBehaviour
{
    public Transform target;

    private NavMeshAgent agent;
    private Transform originalTarget;

    private void Awake()
    {
        TryGetComponent(out agent); //get reference to agent component
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(transform.position, transform.forward * 4, Color.blue); //draw a ray for our AI's 'vision'
        if(target != null)  //if the AI has a target
        {
            agent.SetDestination(target.position); //set destination to target position
            //If raycast hits something, and the current target is not tagged as a door
            if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 4) == true
                && target.tag != "Door") 
            {
                Transform parent = hit.transform.parent; //get the parent of the object we hit
                List<Transform> doors = new List<Transform>(); //create empty list to store doors
                if (parent != null) //if the object has a parent
                {
                    for(int i = 0; i < parent.childCount; i++) //this for loop finds all the doors
                    {
                        if(parent.GetChild(i).tag == "Door") //check to see if child is a door
                        {
                            doors.Add(parent.GetChild(i)); //found door, add it to list of doors in room
                        }
                    }
                    Transform closestDoor = null; //variable to store the closest door
                    for(int i = 0; i < doors.Count; i++) //this for loop finds the closest door
                    {
                        if (closestDoor != null)
                        {
                            //if distance to current door is closer than distance to closest door
                            if (Vector3.Distance(transform.position, doors[i].position)
                                < Vector3.Distance(transform.position, closestDoor.position))
                            {
                                closestDoor = doors[i]; //set closest door to current door
                            }
                        }
                        else //if closest door has not value
                        {
                            closestDoor = doors[i]; //set closest door to current door
                        }
                    }
                    originalTarget = target; //keep track of the current target as the original target
                    target = closestDoor; //set closest door to the current target
                }
            }
            //if distance to target is within stopping distance
            if (Vector3.Distance(transform.position, target.position) <= agent.stoppingDistance)
            {
                if(originalTarget != null) //if an original target is stored
                {
                    target = originalTarget; //reset current target to original target
                    originalTarget = null; //clear original target
                }
                agent.isStopped = true; //AI stops moving
            }
            else 
            {
                agent.isStopped = false; //AI resumes moving
            }
        }
    }
}
